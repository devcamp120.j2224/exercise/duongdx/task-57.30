package com.devcamp.s50.task5730.task5730;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    // Danh sach list student
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Person> getListPerson() {
        ArrayList<Person> persons = new ArrayList<Person>();
        ArrayList<Animals> listPet = new ArrayList<>();
        ArrayList<Subject> listSubject = new ArrayList<>();
        Animals dog = new Animals("dog", "male");
        Animals cat = new Animals("cat", "female");
        listPet.add(dog);
        listPet.add(cat);

        Subject HoaDaiCuong = new Subject("Hoa Hoc ", 32, new Professor("Anh", 40, "male", new Address(), listPet));
        listSubject.add(HoaDaiCuong);
        Student student1 = new Student("Tuan", 26, "male", new Address(), listPet, 2, listSubject);
        Student student2 = new Student("Linh", 23, "female", new Address(), listPet);
        Worker worker1 = new Worker("Duong", 28, "male", new Address("HaiDuong", "ChiLinh", "saoDo", 34), listPet, 20000);
        Professor professor1 = new Professor("Thuan", 32, "male",new Address("VanCao", "HaiPhong", "VietNam", 16), listPet,20000);

        persons.add(student1);
        persons.add(student2);
        persons.add(worker1);
        persons.add(professor1);

        return persons;
    }
}
